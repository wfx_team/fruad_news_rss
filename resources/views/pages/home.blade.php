@extends('layout.main')
@section('title', 'Home')

@section('og-title', 'FraudsBreaking')
@section('og-description', 'FraudsBreaking.com is an online investigative news portal. Explore us for exclusive and Sensational  breaking News.')
@section('og-image', asset('').'assets/img/default-img.jpeg')
@section('og-url', asset(''))


@section('content')
    <div class="row bo-t  bo-b pd-15">
        <div class="col-md-6 bo-r">
            <div class="gazette-post-tag">
                <a href="#">Breaking News</a>
            </div>
            @foreach($newsData->breakingNews as $news)
                <article class="bo-b pd-15 ">
                    <h4 class="">
                        <a href="{{route('site.singleView',['slug'=>$news->slug])}}">{{$news->title}}</a>
                    </h4>
                    <div class="blog-post-thumbnail ">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                             alt="post-thumb">
                    </div>
                    <p class="txt">
                        {{ \Illuminate\Support\Str::limit(strip_tags(strip_tags($news->short_description)),150)}}
                    </p>
                </article>
            @endforeach
            <br>
        </div>
        <div class="col-md-3 bo-r">

            <div class="gazette-post-tag">
                <a href="#">Business</a>
            </div>
            @foreach($newsData->businessNews as $news)
                <article class="heading-1 bo-b">
                    <div class="WSJTheme--headline--7VCzo7Ay ">
                        <h3 class="heading-1">
                            <a class="" href="{{route('site.singleView',['slug'=>$news->slug])}}">{{$news->title}}</a>
                        </h3>
                    </div>
                    <p class="txt">
                        {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}
                        <span class="WSJTheme--stats--2HBLhVc9 "></span>
                    </p>
                    <ul class="WSJTheme--bullets--2pLEdfJV ">
                        <li class="WSJTheme--bullet-item--5c1Mqfdr WSJTheme--icon--1lXhFmSW WSJTheme--none--1l-88_52 ">
                        </li>
                    </ul>
                </article>

            @endforeach
            <br>
            @foreach($newsData->latestNews_body as $news)
                <div class="single-breaking-news-widget">
                    <a class="" href="{{route('site.singleView',['slug'=>$news->slug])}}">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}" alt="">
                        <div class="breakingnews-title">
                            <p>TOP Stories</p>
                        </div>
                        <div class="breaking-news-heading gradient-background-overlay">
                            <h5 class="font-pt">{{$news->title}}</h5>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="col-md-3">
            <div class="subscribe-widget" id="subscribe">
                <p>Subscribe For More Updates</p>
                <div class="subscribe-form">
                    @if (\Illuminate\Support\Facades\Session::has('subs_success'))
                        <div class="alert alert-success text">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_success') !!}
                            </small>
                        </div>
                    @endif
                    @if (\Illuminate\Support\Facades\Session::has('subs_error'))
                        <div class="alert alert-danger">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_error') !!}
                            </small>
                        </div>
                    @endif
                    <form action="{{route('site.subscribe')}}" method="post" id="subs_form">
                        @csrf
                        <input type="email" name="subs_email" id="subs_email" placeholder="Your Email"
                               autocomplete="off" required>
                        <button type="submit" id="subs_btn">subscribe</button>
                    </form>
                </div>
            </div>
            <hr>
            <div class="gazette-post-tag">
                <a href="#">Video</a>
            </div>
            @foreach($newsData->video as $news)
                <div class="gazette-single-catagory-post">
                    <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">
                        <div class="single-catagory-post-thumb mb-15">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>

                        <h5>{{$news->title}}
                        </h5>
                        <span> {{$news->published_date}}</span>
                    </a>
                </div>
            @endforeach


        </div>
    </div>
    <div class="col-md-12">

       <a href="https://www.lamborghini.com/en-en/motorsport/gt3"> <img src="{{asset('assets/img/core-img/banner.jpg')}}" alt="" class="pd-15"></a>

    </div>

    <div class="row bo-t  bo-b pd-15">
        <div class="col-md-3 bo-r">
            <div class="gazette-post-tag">
                <a href="#">Whats happen in india</a>
            </div>
            @foreach($newsData->aniNews as $news)
                <div class="gazette-single-catagory-post">
                    {{--                    <div class="single-catagory-post-thumb mb-15">--}}
                    {{--                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"--}}
                    {{--                             alt="{{$news->title}}">--}}
                    {{--                    </div>--}}

                    <h5><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                    </h5>
                    <span> {{$news->published_date}}</span>
                </div>
            @endforeach

            <div class="gazette-post-tag">
                <a href="#">Science</a>
            </div>
            @foreach($newsData->scienceNews as $news)
                <div class="gazette-single-catagory-post">
                    <div class="single-catagory-post-thumb mb-15">
                        <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                             alt="{{$news->title}}">
                    </div>

                    <h5><a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">{{$news->title}}</a>
                    </h5>
                    <span> {{$news->published_date}}</span>
                </div>
            @endforeach

        </div>
        <div class="col-md-4 bo-r">

            <div class="gazette-todays-post ">
                <!-- Single Today Post -->
                <div class="gazette-post-tag">
                    <a href="#">World</a>
                </div>
                @foreach($newsData->worldNews as $news)
                    <article class="bo-b">
                        <div class="blog-post-thumbnail ">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>
                        <h3 class="heading-2 ">
                            <a class="" href="{{asset('')}}news/{{$news->slug}}">{{$news->title}}</a>
                        </h3>
                        <p class="txt">
                            {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),130)}}
                        </p>
                    </article>
                @endforeach
                <br>
                <div class="gazette-post-tag">
                    <a href="#">Life</a>
                </div>
                <!-- Single Today Post -->
                @foreach($newsData->lifeNews as $news)

                    <article class="heading-1 bo-b">
                        <div class="WSJTheme--headline--7VCzo7Ay ">
                            <h3 class="heading-1">
                                <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt mb-2">{{$news->title}}</a>
                            </h3>
                        </div>
                        <p class="txt">{{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}</p>

                    </article>
                @endforeach

            </div>

        </div>
        <div class="col-md-2 bo-r">

            <div class="gazette-todays-post ">
                <div class="donnot-miss-widget">
                    <div class="gazette-post-tag">
                        <a href="#">entertainment</a>
                    </div>
                    @foreach($newsData->entertainmentNews as $news)
                        <div class="gazette-single-catagory-post">
                            <a href="{{asset('')}}news/{{$news->slug}}" class="font-pt">
                                <div class="single-catagory-post-thumb mb-15">
                                    <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                         alt="{{$news->title}}">
                                </div>

                                <h5>{{$news->title}}
                                </h5>
                                <span>{{$news->published_date}}</span>
                            </a>
                        </div>

                    @endforeach
                </div>

            </div>

        </div>
        <div class="col-md-3">
            <div class="gazette-post-tag">
                <a href="#">Health</a>
            </div>

            @foreach($newsData->healthNews as $news)
                <article class="heading-1 bo-b">
                    <div class="WSJTheme--headline--7VCzo7Ay ">
                        <h3 class="heading-1">
                            <a class="" href="{{asset('')}}news/{{$news->slug}}">{{$news->title}}</a></h3>
                    </div>
                    <p class="txt">
                        {{\Illuminate\Support\Str::limit(strip_tags($news->short_description),100)}}
                    </p>
                </article>

        @endforeach

        <!-- Advert Widget -->
            <div class="advert-widget">

                <div class="advert-thumb mb-30">
                    <a href="https://tesla.com"><img src="{{asset('')}}assets/img/bg-img/add.png" alt=""></a>
                </div>
            </div>


        </div>
    </div>

@endsection

@section('extra-js')
    <script>

    </script>
@endsection
