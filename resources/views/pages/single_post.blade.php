@extends('layout.main')
@section('title', $news->title)
@section('og-title', $news->title)
@section('og-description', $news->title)
@section('og-image-facebook', (($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg') )
@section('og-image', asset('').'assets/img/twitter-default.jpg')
@section('og-url', asset('').'news/'.$news->slug)


@section('content')
    <div class="row bo-t  bo-b pd-15">

        <div class="col-md-9 bo-r">

            {{--<div class="gazette-post-tag">--}}
            {{--<a href="#">Politices</a>--}}
            {{--</div>--}}
            <h2 class="singlehead">{{$news->title}}</h2>
            <p class="gazette-post-date">{{$news->published_date}}</p>
            <!-- Post Thumbnail -->


            <br>
            @if($news->source == 'admin' AND $news->source_type != 'Video')
                <div class="blog-post-thumbnail ">
                    <img src="{{$news->thumb}}" alt="post-thumb">
                </div>
            @else
                @if($news->source_type == 'Video')
                    <style>
                        .embed-container {
                            position: relative;
                            padding-bottom: 56.25%;
                            height: 0;
                            overflow: hidden;
                            max-width: 100%;
                        }

                        .embed-container iframe, .embed-container object, .embed-container embed {
                            position: absolute;
                            top: 0;
                            left: 0;
                            width: 100%;
                            height: 100%;
                        }
                    </style>
                    <div class='embed-container'>
                        <iframe src='{{$news->original_url}}' frameborder='0' allowfullscreen></iframe>
                    </div>
                @endif
            @endif
            <div class="txtdiv mt-5 ">
                <p class="singletxt">
                    @if($news->source == 'admin' AND $news->source_type == 'Breaking News')
                        {!! $news->long_description !!}
                    @else
                        {!! base64_decode($news->long_description) !!}
                    @endif

                </p>

            </div>
            <div class="post-continue-reading-share d-sm-flex align-items-center justify-content-between mt-30">
                <div class="post-continue-btn">
                </div>
                <div class="post-share-btn-group">
                    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="subscribe-widget">
                <div class="widget-title">
                    <h5>subscribe</h5>
                </div>
                <div class="subscribe-form">
                    @if (\Illuminate\Support\Facades\Session::has('subs_success'))
                        <div class="alert alert-success text">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_success') !!}
                            </small>
                        </div>
                    @endif
                    @if (\Illuminate\Support\Facades\Session::has('subs_error'))
                        <div class="alert alert-danger">
                            <small>
                                {!! \Illuminate\Support\Facades\Session::get('subs_error') !!}
                            </small>
                        </div>
                    @endif
                    <form action="{{route('site.subscribe')}}" method="post" id="subs_form">
                        @csrf
                        <input type="email" name="subs_email" id="subs_email" placeholder="Your Email"
                               autocomplete="off" required>
                        <button type="submit" id="subs_btn">subscribe</button>
                    </form>
                </div>
            </div>
            <div class=" bo-t">
                <div class="advert-thumb mb-30">
                    <a href="#"><img src="{{asset('')}}assets/img/bg-img/add.png" alt=""></a>
                </div>
            </div>
            <br>
            <div class="gazette-single-catagory-post">
                <div class="gazette-post-tag">
                    <a href="#">breaking News</a>
                </div>
            @foreach($breaking as $news)
                <!-- Single Catagory Post -->
                    <div class="gazette-single-catagory-post">
                        <h5><a href="{{route('site.singleView',['slug'=>$news->slug])}}"
                               class="font-pt">{{$news->title}}</a></h5>
                        <span>{{$news->published_date}}</span>
                    </div>
                @endforeach

            </div>
            <div class="donnot-miss-widget">
                <div class="widget-title">
                    <h5>Top Stories</h5>
                </div>
            @foreach($latest_news as $news)
                <!-- Single Don't Miss Post -->
                    <div class="single-dont-miss-post d-flex mb-30">
                        <div class="dont-miss-post-thumb">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="">
                        </div>
                        <div class="dont-miss-post-content">
                            <a href="{{route('site.singleView',['slug'=>$news->slug])}}"
                               class="font-pt">{{$news->title}}</a>
                            <span>{{$news->published_date}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="row bo-t  bo-b pd-15">
            <div class="col-md-4 bo-r">
                <h2 class="cn_title">Entertainment</h2>
                <div class="column zn__column--idx-0">
                    <ul class="cn cn-list-xs cn--idx-0 cn-container_ED03BAA0-E1BF-D5E4-2FF0-E6CF17C3C0B4 cn--expandable cn--collapsed"
                        data-layout="list-xs" data-bundle="listexpandable">
                        @foreach($entertainmentNews as $news)
                            <li>
                                <article class="">
                                    <div class="WSJTheme--headline--7VCzo7Ay ">
                                        <p class="heading-11">
                                            <a class=""
                                               href="{{route('site.singleView',['slug'=>$news->slug])}}">
                                                {{$news->title}}
                                            </a>
                                        </p>
                                    </div>

                                </article>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
            <div class="col-md-4 bo-r">
                <h2 class="cn_title">World</h2>
                @foreach($world as $news)
                    <div class="gazette-single-todays-post d-md-flex align-items-start bo-b ">
                        <div class="todays-post-thumb">
                            <img src="{{(($news->thumb)?$news->thumb :asset('').'assets/img/default-img.jpeg')}}"
                                 alt="{{$news->title}}">
                        </div>
                        <div class="todays-post-content">

                            <p>
                                <a href="{{route('site.singleView',['slug'=>$news->slug])}}"
                                   class="font-pt mb-2">{{$news->title}}</a>
                            </p>

                        </div>
                    </div>
                @endforeach


            </div>


            <div class="col-md-4">
                <h2 class="cn_title">Business</h2>
                <div class="column zn__column--idx-0">
                    <ul class="cn cn-list-xs cn--idx-0 cn-container_ED03BAA0-E1BF-D5E4-2FF0-E6CF17C3C0B4 cn--expandable cn--collapsed"
                        data-layout="list-xs" data-bundle="listexpandable">
                        @foreach($business as $news)
                            <li>
                                <article class="">
                                    <div class="WSJTheme--headline--7VCzo7Ay ">
                                        <p class="heading-11">
                                            <a class=""
                                               href="{{route('site.singleView',['slug'=>$news->slug])}}">
                                                {{$news->title}}
                                            </a>
                                        </p>
                                    </div>

                                </article>
                            </li>
                        @endforeach

                    </ul>
                </div>


            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <style>
        .story-body__h1 {
            display: none;
        }

        .story-body__mini-info-list-and-share-row {
            display: none;
        }

        .story-body__unordered-list {
            display: none;
        }

        .story-more {
            display: none;
        }

        .share--lightweight {
            display: none;
        }

        #topic-tags {
            display: none;
        }

        #hearken-curiosity-6532 {
            display: none;
        }
    </style>

@endsection
