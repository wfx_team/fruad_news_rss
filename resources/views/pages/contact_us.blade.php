@extends('layout.main')
@section('title', 'Contact Us')
@section('content')
<!-- Breadcumb Area Start -->
    <div class="breadcumb-area section_padding_50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breacumb-content d-flex align-items-center justify-content-between">
                        <h3 class="font-pt mb-0">Contact</h3>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area End -->

    <section class="gazette-contact-area section_padding_100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="gazette-heading">
                        <h4 class="font-bold">Contact Us Now</h4>
                    </div>
                       @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                @endif
 <!-- Contact Form -->
                    <form action="{{route('site.sendContactUs')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <select id="salutations" class="form-control" required name="data[salutations]">
                                <option value="mr" selected>Mr</option>
                                <option value="mrs">Mrs</option>
                                <option value="miss" >Miss</option>
                                <option value="dr" >Dr</option>
                            </select>
                        </div>
                         <div class="form-group">
                            <input type="text" class="form-control" name="data[name]" id="name" required
                                   placeholder="Enter Your Full Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="data[email]" required id="contact-email"
                                   placeholder="Email">
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" name="data[address]" required id="contact-address"
                                   placeholder="Full Address">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[subject]" required id="contact-subject"
                                   placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="data[msg]" cols="30" required rows="10"
                                      placeholder="Message"></textarea>
                        </div>
                        <button type="submit" class="btn contact-btn">SUBMIT <i class="fa fa-angle-right ml-2"></i>
                        </button>
                    </form>
                </div>
                <div class="col-12 col-md-4">

                    <div class="gazette-heading">
                        <h4 class="font-bold">Email</h4>
                    </div>
                    <div class="contact-address-info">
                        <p>info@fraudsbreaking.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

