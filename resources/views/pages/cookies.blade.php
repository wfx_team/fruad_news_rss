@extends('layout.main')
@section('title', 'Cookies')
@section('content')

    <section class="gazette-about-us-area section_padding_100_70">
        <div class="about-us-content">
            <div class="container">
                <div class="row">


                    <div class="col-12 col-md-12">

                        <div class="breacumb-content d-flex align-items-center justify-content-between">


                        </div>
                        <article id="2951cf5e-8cae-415c-89c3-c14a231f8640" class="gel-explainer-ops" style="min-height: 216px;">
                            <h1 class="gel-trafalgar-bold utb-heading-color">What do I need to know about cookies?</h1>


                            <!-- explainer-content -->


                            <div class="">
                                <div class="gel-explainer--generic">

                                    <div class="gel-copy">
                                        <p>Cookies are small text files which are transferred to your computer or mobile when you visit a website or app.</p>
                                        <p>We use them to:</p>
                                        <ul>
                                            <li>Remember information about you, so you don’t have to give it to us again. And again. And again</li>
                                            <li>Keep you signed in, even on different devices</li>
                                            <li>Help us understand how people are using our services, so we can make them better</li>

                                            <li>Help us personalise the Scamsbreaking to you by remembering your preferences and settings. And your progress, so you can pause and pick up where you left off watching a programme, even on a different device</li>
                                            <li>To find out if our emails have been read and if you find them useful</li>
                                        </ul>
                                        <p>You can <a href="#">change your Scamsbreaking cookie settings</a> at any time. And you can find out how to <a href="#https://www.Scamsbreaking.co.uk/usingtheScamsbreaking/cookies/how-can-i-stop-my-internet-browser-tracking-my-info/">stop your browser tracking your info here</a>.</p>
                                        <p>There are different types of cookies:</p>
                                    </div>
                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="c9e77e693f22d5c608373254a030f410">
                                <h2 class="gel-great-primer-bold"><a href="##c9e77e693f22d5c608373254a030f410" class="toggle open" data-id="c9e77e693f22d5c608373254a030f410">First-party cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These cookies are set by the website you’re visiting. And only that website can read them.</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="4568fdc96072b759f84e66db6f089095">
                                <h2 class="gel-great-primer-bold"><a href="##4568fdc96072b759f84e66db6f089095" class="toggle open" data-id="4568fdc96072b759f84e66db6f089095">Third-party cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These cookies are set by someone other than the owner of the website you’re visiting. Some Scamsbreaking web pages may also contain content from other sites like YouTube or Flickr, which may set their own cookies. Also, if you share a link to a Scamsbreaking page, the service you share it on (for example, Facebook) may set a cookie on your browser. We have no control over third-party cookies - <a href="#http://www.Scamsbreaking.co.uk/usingtheScamsbreaking/cookies/what-happens-if-third-party-cookies-are-disabled-on-my-browser/">you can turn them off</a>, but not through us.</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="656598ff5e9df8117cfcc4ffdc7d8717">
                                <h2 class="gel-great-primer-bold"><a href="##656598ff5e9df8117cfcc4ffdc7d8717" class="toggle open" data-id="656598ff5e9df8117cfcc4ffdc7d8717">Session cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These cookies only last as long as your online session, and disappear from your computer or device when you close your browser (like Internet Explorer or Safari).</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="cbe34e8f914ee74aa902e60bfc80b4f0">
                                <h2 class="gel-great-primer-bold"><a href="##cbe34e8f914ee74aa902e60bfc80b4f0" class="toggle open" data-id="cbe34e8f914ee74aa902e60bfc80b4f0">Persistent cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These cookies stay on your computer or device after your browser has been closed and last for a time specified in the cookie. We use persistent cookies when we need to know who you are for more than one browsing session. For example, we use them to remember your preferences for the next time you visit.</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="028d4ef3978d66c9c9775188df6b5a7e">
                                <h2 class="gel-great-primer-bold"><a href="##028d4ef3978d66c9c9775188df6b5a7e" class="toggle open" data-id="028d4ef3978d66c9c9775188df6b5a7e">Strictly necessary cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These cookies let you use all the different parts of the Scamsbreaking website. Without them, services that you’ve asked for can’t be provided. Also, as a public service, we collect data from you to help us understand how people are using the Scamsbreaking online, so we can make it better. We sometimes get other companies to analyse how people are using the Scamsbreaking online.</p>
                                        <p>Some examples of how we use these cookies are:</p>
                                        <ul>
                                            <li>When you sign in to the Scamsbreaking</li>
                                            <li>Remembering security settings that affect access to certain content, for example, any parental controls</li>
                                            <li>Collecting information on which web pages visitors go to most often so we can improve our online services</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="80323fc65493c18f27173d21f97e941f">
                                <h2 class="gel-great-primer-bold"><a href="##80323fc65493c18f27173d21f97e941f" class="toggle open" data-id="80323fc65493c18f27173d21f97e941f">Functional cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These help us personalise the Scamsbreaking to you by remembering your preferences and settings. Some examples of how we use these cookies are:</p>
                                        <ul>
                                            <li>Remembering your choice of playlists and favourite content and help you do things like commenting on a blog</li>
                                            <li>Remembering where you paused a programme on iPlayer and then later picking up where you left off</li>
                                            <li>Remembering if you visited the website before so that messages for new visitors are not displayed to you</li>
                                            <li>Remembering settings on the website like colour, font size and layout</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="8c1778a47c661bee5ce4ea4ca5e313ba">
                                <h2 class="gel-great-primer-bold"><a href="##8c1778a47c661bee5ce4ea4ca5e313ba" class="toggle open" data-id="8c1778a47c661bee5ce4ea4ca5e313ba">Performance cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>These help us&nbsp;make sure that the website is working properly and fix any errors. And they let us try out different ideas.</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="3d92fd5ab89ddc19ab43a885abcccbf8">
                                <h2 class="gel-great-primer-bold"><a href="##3d92fd5ab89ddc19ab43a885abcccbf8" class="toggle open" data-id="3d92fd5ab89ddc19ab43a885abcccbf8">Advertising cookies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>Some websites use advertising networks to show you specially targeted adverts when you visit. These networks may also be able to track your browsing across different sites. We don’t set advertising cookies for people in the UK. Scamsbreaking Studios sites like Scamsbreaking.com do use advertising cookies but they won’t track your browsing outside the Scamsbreaking. You can find more information <a href="#https://www.Scamsbreaking.co.uk/usingtheScamsbreaking/cookies/how-does-the-Scamsbreaking-use-cookies-for-advertising/">here</a> about how the Scamsbreaking uses advertising cookies.</p>
                                    </div>

                                </div>
                            </div>



                            <div class="gel-explainer--diagnostic gel-pica open" id="e6ef8547abd59887096a03ceaf10c57a">
                                <h2 class="gel-great-primer-bold"><a href="##e6ef8547abd59887096a03ceaf10c57a" class="toggle open" data-id="e6ef8547abd59887096a03ceaf10c57a">Other tracking technologies<span class="gel-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#/usingtheScamsbreaking/static/images/gel-icons-core-set.svg#gel-icon-down"></use></svg></span></a></h2>
                                <div class="expand">

                                    <div class="gel-copy">
                                        <p>Some sites use things like web beacons, clear GIFs, page tags and web bugs to understand how people are using them and to target advertising to them.</p>
                                        <p>They usually take the form of a small, transparent image that is embedded in a web page or email. They work with cookies and capture data like your IP address, when you viewed the page or email, what device you were using and where you were. You can <a href="#https://www.Scamsbreaking.co.uk/usingtheScamsbreaking/cookies/how-can-i-stop-my-internet-browser-tracking-my-info/">find out how to avoid them here</a>.</p>
                                    </div>

                                </div>
                            </div>





                        </article>




                        {{--Open Multiple URLs | Compress Videos | Online PDF Converter--}}
                        {{--© 2018 Unminify2.com | Privacy--}}


                    </div>

                </div>
            </div>
        </div>


    </section>

@endsection
