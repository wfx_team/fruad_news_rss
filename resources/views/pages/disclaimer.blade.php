@extends('layout.main')
@section('title', 'Disclaimer')
@section('content')
    <!-- Breadcumb Area End -->
    <section class="gazette-about-us-area section_padding_100_70">
        <div class="about-us-content">
            <div class="container">
                <div class="row">


                    <div class="col-12 col-md-12">
                        <div class="policy-misc">
                            <p>The content on this site, including news, quotes, data and other information, is provided by Froudsbreaking.com and its third party content providers for your personal information only, and is not intended for trading purposes. Content on this site is
                                not appropriate for the purposes of making a decision to carry out a transaction or trade. Nor does it provide any form of advice (investment, tax, legal) amounting to investment advice, or make any recommendations regarding particular financial
                                instruments, investments or products.</p>

                            <p>This site does not provide investment advice nor recommendations to buy or sell securities. We do not request personal information in any unsolicited email correspondence with our customers. Any correspondence offering trading advice or unsolicited
                                message asking for personal details should be treated as false and reported to   Froudsbreaking.com.</p>

                            <p>Neither Froudsbreaking.com nor its third party content providers shall be liable for any errors, inaccuracies or delays in content, or for any actions taken in reliance thereon.  Froudsbreaking.com EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESSED OR IMPLIED,
                                AS TO THE ACCURACY OF ANY THE CONTENT PROVIDED, OR AS TO THE FITNESS OF THE INFORMATION FOR ANY PURPOSE.</p>

                            <p>Although  Froudsbreaking.com makes reasonable efforts to obtain reliable content from third parties,  Froudsbreaking.com does not guarantee the accuracy of or endorse the views or opinions given by any third party content provider. This site may point to
                                other Internet sites that may be of interest to you, however  Froudsbreaking.com does not endorse or take responsibility for the content on such other sites.</p>

                            <h4> Froudsbreaking.com Newsletters:</h4>
                            <p>Whilst  Froudsbreaking.com has used reasonable endeavours to ensure that the information provided by  Froudsbreaking.com in the newsletters is accurate and up to date as at the time of issue, it reserves the right to make corrections and does not warrant
                                that it is accurate or complete. News will change with time. Froudsbreaking.com hereby disclaims all liability to the maximum extent permitted by law in relation to the newsletters and does not give any warranties (including any statutory ones) in
                                relation to the news. This is a free service and therefore you agree by receiving any newsletter(s) that this disclaimer is reasonable. Any copying, redistribution or republication of  Froudsbreaking.com newsletter(s), or the content thereof, for
                                commercial gain is strictly prohibited.</p>

                            <h4>Third party legal notices:</h4>
                            <p>The Hong Kong Stock Exchange quotes are the copyright of The Stock Exchange of HK Limited. It endeavours to ensure the accuracy and reliability of theinformation provided but does not guarantee its accuracy or reliability and accepts no liability
                                (whether in tort or contract or otherwise) for any loss ordamage arising from inaccuracies or omissions.</p>

                            <h4>.OSEAX Disclaimer and Copyright Notice</h4>

                            <p><strong>Disclaimer</strong>: The information is supplied by Oslo Bors Informasjon AS (OBI). The content of this web-site is intended as a service to the market only. OBI disclaims all responsibility for any and all mistakes or inaccuracies in the
                                information. Further, OBI disclaims all liability for loss or damage, which may result form the use of information on this web-site.</p>

                            <p><strong>Copyright Notice</strong>: The proprietors of any and all material found on this web-site is Oslo Bors Information AS (OBI). The material is protected by the provisions of the Norwegian Copyright Act (Andsverkloven), and OBI has exclusive
                                rights to dispose of the material found on the web-site. The contents may not be made available outside of the private sphere or copied for a purpose other than for private use or as prescribed by involuntary statute, except with OBI's written
                                consent.</p>

                            <p>As copying is also considered downloading and storage on any computer, disk or other device designed to reproduce the material. As making the content available is considered active transmittal or handling over of the materials to others, as well as
                                placing the material at the disposal of other.</p>

                            <p>The material may be read on-screen, on-spot and on-line.</p>

                            <h4>BOAT Terms and Conditions of use</h4>

                            <p>1. <strong>License</strong>. Customer and its End Users are granted a personal, non-exclusive, revocable, non-transferable, non sub-licensable, license to use the Services for its internal business purposes. <strong>"End User"</strong> means any individual
                                recipient of the Services who is from time to time authorized by Customer to access and use the Services.</p>
                            <p>
                            </p>
                            <p>2. <strong>Term and Termination</strong>. The term and termination provisions relating to Customer's access to, and use of, the Services, are as agreed between Customer and Vendor, provided Customer's access to the Services may be terminated or suspended
                                immediately in respect of any breach of these terms by Customer and/or End User.</p>
                            <p>3. <strong>Prohibited Uses</strong>. distributing, transferring, sub-licensing, renting, lending, transmitting, selling, re-circulating, repackaging, assigning, leasing, reselling, publishing or making available all or any portion of the Services;</p>
                            <p>3.2 using the Services to develop, create or directly price any index (e.g., any composite financial index), data feed services, valuations services to third parties or any database or service that competes directly with the Services or any other
                                BOAT service offered in the market place or that would create a substitute for any such BOAT service. For the avoidance of doubt, the Services shall not be used in any index or to create, price or calculate indices or any similar derivative product
                                (including but not limited to "baskets" and "strategies"). For example, a total return index on the trading strategy or total return of investment strategy in relation to a structured product whose performance is based on a quantitative trading
                                strategy, and the daily/weekly NAV or redemption value together with the Services are used to calculate such index; and</p>
                            <p>3.3 using the Services for any illegal purpose or contrary to the laws applicable in the jurisdictions where Customer or End Users operates or applicable to supply and use of the Services.</p>
                            <p>4. <strong>Limited Permitted Use</strong>. Customer and End User are permitted to reproduce non-systematic and limited excerpts from Services in documents for distribution to their clients or potential clients in graphical format only for viewing,
                                not reproduced or republished by the Customer and End User in any format that would enable the recipient to incorporate the Services in a database of their own. Customer and End User may also create and include "Derived Data" in its periodic or
                                ad-hoc reports to its clients. <strong>"Derived Data"</strong> shall mean, for the purposes of these terms and conditions only, data that Customer and/or End User have developed through a process in conjunction with additional third party data
                                and professional experience, such that the Derived Data: (i) cannot be identified or reverse-engineered as originating or directly derived from the Services; (ii) includes an amount of the Services that has no independent commercial value; (iii)
                                is not separately marketed by Customer; (iv) could not be used by its recipients as a substitute for the Services or any part of it; (v) has no commercial value in its own right, irrespective of whether that value is realised by the Customer or
                                not; and (vi) the distribution of Derived Data is only of supportive nature to Customer and End User's services (for example - report on End User's performance) and does not constitute a service that End User's clients may subscribe for.</p>
                            <p>5. <strong>Intellectual Property</strong>. All copyright, database rights, trade marks, patents, rights of privacy or publicity and other proprietary or intellectual property rights (including all models, software, data and any materials) comprised
                                in all or any of the Services, or their provision, and all enhancements, modifications or additional services thereto, are and will be the exclusive property of BOAT. Customer will not use the same (including copying, reverse engineering or disclosing
                                it to any person, for any purpose whatsoever) and will not remove or deface any trademarks associated with the Services;</p>
                            <p>6. <strong>Disclaimer</strong>. The Services distributed to Customer and End User shall be on "as is" basis. Neither Vendor, BOAT, its affiliates nor any third party data provider makes any warranty, express or implied, as to the accuracy or completeness
                                of the Services or as to the results to be attained by Customer and End User or others from the use of the Services. Customer and its End Users hereby acknowledge that there are no express or implied warranties of title, merchantability or fitness
                                for a particular purpose or use, and that it has not relied upon any warranty, guaranty or representation made by BOAT, its affiliates or any Data Provider. The Customer and End User are bound to any legends, disclaimers, and notices appearing
                                from time to time in the Services;</p>
                            <p>7. <strong>Liability</strong>. Neither Vendor, BOAT, its affiliates nor any Data Provider shall in any way be liable to Customer and End User or any client of Customer and End User for any inaccuracies, errors or omissions, regardless of cause, in
                                the Services provided hereunder or for any damages (whether direct or indirect) resulting therefrom. Without limiting the foregoing, BOAT shall have no liability whatsoever to Customer and End User, whether in contract (including under an indemnity),
                                in tort (including negligence), under a warranty, under statute or otherwise, in respect of any loss or damage suffered Customer and by End User as a result of or in connection with any opinions, recommendations, forecasts, judgments, or any other
                                conclusions, or any course of action determined, by Customer and End User or any client of Customer and End User, whether or not based on the Services.</p>
                            <p>8. <strong>Indemnity</strong>. Customer will indemnify, defend and hold harmless BOAT and its affiliates from and against any and all losses, liabilities, damages, costs brought by any third party accessing of all or part of the Services through or
                                by means of Subscriber.</p>
                            <p>9. <strong>Miscellaneous</strong>. The terms and conditions herein, solely in connection with the Services:</p>
                            <p>9.1 shall supersede any other terms between Customer and Vendor;</p>
                            <p>9.2 shall be for the benefit of BOAT, its data providers and its and their respective affiliates;</p>
                            <p>9.3 may not be amended unless agreed to in writing by BOAT; and</p>
                            <p>9.4 shall be governed by and construed under the laws of England and Wales and each party submits to the exclusive jurisdiction of the courts of England and Wales.</p>

                            <script type="javascript">
                                var year = new Date() document.write('
                                    <p>&copy; Froudsbreaking.com ' + year.getFullYear() + '</p>');
                            </script>
                        </div>

                    </div>

                </div>
            </div>
        </div>


    </section>
@endsection
