<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta property="og:title" content="@yield('og-title')">
    <meta property="og:description" content="@yield('og-description')">
    <meta property="og:image" content="@yield('og-image-facebook')">
    <meta property="og:url" content="@yield('og-url')">

    <meta name="twitter:title" content="@yield('og-title')">
    <meta name="twitter:description" content="@yield('og-description')">
    <meta name="twitter:image" content="@yield('og-image')">
    <meta name="twitter:card" content="summary_large_image">


    <!-- Title  -->
    <title>@yield('title') - {{ config('app.name') }} </title>

    <!-- Favicon  -->
    <link rel="icon" href="{{asset('')}}assets/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{asset('')}}assets/css/core-style.css">
    <link rel="stylesheet" href="{{asset('')}}assets/style.css">

    <!-- Responsive CSS -->
    <link href="{{asset('')}}assets/css/responsive.css" rel="stylesheet">
    @yield('extra-css')
</head>

<body onload="startTime()">
<!-- Header Area Start -->
<header class="header-area">

    <!-- Middle Header Area -->
    <div class="middle-header">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Logo Area -->
                <div class="col-12 col-md-12 text-center">
                    <div class="logo-area">
                        <a href="{{route('site.index')}}"><img src="{{asset('')}}assets/img/core-img/logo.png"
                                                               alt="logo"></a>
                    </div>
                </div>
                <!-- Header Advert Area -->
            </div>
        </div>
    </div>
    <!-- Bottom Header Area -->
    <div class="bottom-header">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="main-menu">
                        <nav class="navbar navbar-expand-lg">
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#gazetteMenu" aria-controls="gazetteMenu" aria-expanded="false"
                                    aria-label="Toggle navigation"><i class="fa fa-bars"></i> Menu
                            </button>
                            <div class="collapse navbar-collapse" id="gazetteMenu">
                                <ul class="navbar-nav mr-auto">
                                    @foreach($categories as $category )
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="{{route('site.categoryView',['slug'=>$category->slug])}}">{{$category->name}}</a>
                                        </li>
                                    @endforeach
                                    <li class="nav-item">
                                        <ul class="social-network social-circle">

                                            <li><a href="https://www.facebook.com/Frauds-Breaking-108833054359982" class="ico icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/BreakingFrauds" class="ico icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
{{--                                            <li><a href="https://instagram.com/scamsbreaking" class="ico icoTwitter" title="Twitter"><i class="fa fa-instagram"></i></a></li>--}}


                                        </ul>
                                    </li>
                                </ul>
                                <!-- Search Form -->
                                <div class="header-search-form mr-auto">
                                    <form action="{{route('site.search')}}">
                                        <input type="search" placeholder="Input your keyword then press enter..."
                                               id="search" name="s">
                                        <input class="d-none" type="submit" value="submit">
                                    </form>
                                </div>
                                <!-- Search btn -->
                                <div id="searchbtn">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Header Area End -->
<div class="container-fluid">
