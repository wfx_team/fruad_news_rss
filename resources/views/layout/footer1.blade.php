</div>
<style>
    .footer-widget-menu{
        width: 100%;
        float: left;
    }
</style>
<footer class="footer-area "
        style="background-color:#000;">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0&appId=874679096064914&autoLogAppEvents=1"
            nonce="asaKsOCd"></script>
    <!-- Top Footer Area -->
    <div class="top-footer-area pd-15">
        <div class="container-fluid">
            <div class="row">
                <!-- Single Footer Widget -->

                <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt text-uppercase">Categories</h4>
                                </div>

                                @foreach($categories->chunk(12) as $one )
                                    <ul class="footer-widget-menu">
                                        @foreach($one as $category)
                                            <li>
                                                <a href="{{route('site.categoryView',['slug'=>$category->slug])}}">{{$category->name}}</a>
                                            </li>
                                        @endforeach

                                    </ul>

                                @endforeach


                            </div>
                        </div>
                        <!-- Single Footer Widget -->
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt text-uppercase">Pages</h4>
                                </div>
                                <ul class="footer-widget-menu">
                                    <li><a href="{{asset('')}}about-us">About Us</a></li>
                                    <li><a href="{{asset('')}}contact-us">Contact Us</a></li>
                                    <li><a href="{{asset('')}}privacy-policy">Privacy Policy</a></li>
                                    <li><a href="{{asset('')}}advertise-with-us">Advertise with Us</a></li>
                                    <li><a href="{{asset('')}}disclaimer">Disclaimer</a></li>
                                    <li><a href="{{asset('')}}terms">Terms of use</a></li>
                                    </li>
                                    <li><a href="">Submit Your Story</a></li>
                                    </li>
                                    <li><a href="">Sitemap</a></li>
                                    </li>
                                    <li><a href="#subscribe">Subscribe </a></li>
                                    </li>
                                    <li><a href="{{asset('')}}podcast">Podcast</a></li>
                                    </li>
                                    <li><a href="{{asset('')}}cookies">Cookies</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">

                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">
                                    <li><a href="https://www.interpol.int/en/Who-we-are/Member-countries/Africa/ALGERIA">  Algeria</a></li>
                                    <li><a href=" https://www.angolain.org/department/index.php?structureid=21"> Angola </a></li>

                                    <li><a href=" https://gov.bw/ministries/botswana-police-service"> Botswana  </a></li>
                                    <li><a href=" https://www.police.gov.bf/"> Burkina Faso </a></li>
                                    <li><a href=" https://apcof.org/country-data/burundi/"> Burundi  </a></li>
                                    <li><a href="https://www.dgsn.cm/en/ "> Cameroon  </a></li>
                                    <li><a href=" https://www.osac.gov/Country/CaboVerde/Content/Detail/Report/33002b2a-1d03-4702-afe8-18d3928452a5"> Cape Verde   </a></li>
                                    <li><a href="https://www.interpol.int/en/Who-we-are/Member-countries/Africa/CENTRAL-AFRICAN-REPUBLIC ">  Africa   </a></li>
                                    <li><a href=" https://www.interpol.int/en/Who-we-are/Member-countries/Africa/CHAD"> Chad </a></li>
                                    <li><a href=" https://www.interpol.int/en/Who-we-are/Member-countries/Africa/COMOROS ">Comoros  </a></li>
                                    <li><a href="https://www.rbp.gov.bt/ ">Bhutan  </a></li>

                                </ul>


                            </div>
                        </div>

                        <!-- Single Footer Widget -->
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">
                                    <li><a href=" https://cbi.gov.in/ "> India </a></li>
                                    <li><a href="https://moi.gov.af/en "> Afghanistan</a></li>
                                    <li><a href="https://www.policemc.gov.bh/en/ "> Baharain  </a></li>
                                    <li><a href=" https://www.police.gov.bd/ "> Bangladesh </a></li>

                                    <li><a href=" http://www.police.gov.bn/Theme/Home.aspx"> Brunei </a></li>
                                    <li><a href=" http://cambodiapolice.com/ "> Cambodia </a></li>
                                    <li><a href="https://www.interpol.int/en/Who-we-are/Member-countries/Asia-South-Pacific/CHINA "> China  </a></li>
                                    <li><a href="https://www.npa.gov.tw/NPAGip/wSite/mp?mp=4"> Taiwan </a></li>
                                    <li><a href=" https://www.police.lk/ "> Sri Lanka  </a></li>
                                    <li><a href=" http://www.aseanapol.org/information/indonesian-national-police"> Indonesia  </a></li>
                                    <li><a href=" http://police.ir/"> Iran </a></li>


                                </ul>



                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">

                                    <li><a href="https://polis.osce.org/country-profiles/bulgaria "> Bulgaria</a></li>
                                    <li><a href="https://www.politi.dk/da/servicemenu/forside/ "> Denmark  </a></li>
                                    <li><a href=" http://psgtroopers.com/"> Philippines </a></li>
                                    <li><a href="http://police.gov.sg/"> Singapore</a></li>
                                    <li><a href=" https://www.bka.de/"> Georgea </a></li>
                                    <li><a href=" http://www.astynomia.gr/ "> Greece </a></li>
                                    <li><a href="https://www.poliziadistato.it "> Italy  </a></li>
                                    <li><a href="http://policija.lrv.lt/en"> Lithuania </a></li>
                                    <li><a href=" www.marechaussee.nl/ ">Netherlands </a></li>
                                    <li><a href="https://www.politiet.no/politidirektoratet/"> Norway  </a></li>
                                    <li><a href=" http://www.policja.pl/portal/pol/90/English_version.html"> Poland </a></li>


                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">

                                    <li><a href="http://www.mai.gov.pt/">Portugal</a></li>
                                    <li><a href="https://www.politiaromana.ro/">Romania</a></li>
                                    <li><a href="https://en.mvd.ru/">Russia</a></li>

                                    <li><a href="https://www.politiaromana.ro/">Serbia</a></li>
                                    <li><a href="https://www.policija.si/eng/">Slovenia</a></li>
                                    <li><a href="http://www.guardiacivil.es/">Spain</a></li>
                                    <li><a href="http://nfc.polisen.se/">Sweden</a></li>
                                    <li><a href="http://www.ezv.admin.ch/org/04135/04138/index.html?lang=en">Switzerland</a></li>
                                    <li><a href="http://www.egm.gov.tr/">Turkey</a></li>
                                    <li><a href="http://www.npu.gov.ua/en/">Ukraine</a></li>

                                    <li><a href="https://en.wikipedia.org/wiki/Military_ranks_of_Bahamas">	Bahamas</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">

                                    <li><a href="https://en.wikipedia.org/wiki/Military_ranks_of_Belize">Belize</a></li>
                                    <li><a href="http://www.bermudapolice.bm/">Bermuda</a></li>
                                    <li><a href="http://www.viarail.ca/en/">Canada</a></li>

                                    <li><a href="https://web.archive.org/web/20080606045624/http://www.jcf.gov.jm/">Jamaica</a></li>
                                    <li><a href="https://www.usmarshals.gov/">Mexico</a></li>
                                    <li><a href="http://www.policia.gob.pa/">Panama</a></li>

                                    <li><a href="https://www.afp.gov.au/">Australia</a></li>
                                    <li><a href="http://www.police.gov.fj/">Fiji</a></li>
                                    <li><a href="http://www.police.govt.nz/">New Zealand</a></li>
                                    <li><a href="http://www.policia.bo/">Bolivia</a></li>
                                    <li><a href="http://www.dpf.gov.br/">	Brazil</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                            <div class="single-footer-widget">
                                <div class="footer-widget-title">
                                    <h4 class="font-pt">LEA</h4>
                                </div>
                                <ul class="footer-widget-menu">

                                    <li><a href="http://www.carabineros.cl/">Chile</a></li>
                                    <li><a href="http://www.policia.gov.co/">Colombia</a></li>
                                    <li><a href="http://www.npsc.go.jp/">Japan</a></li>

                                    <li><a href="http://www.kenyapolice.go.ke/">Kenya</a></li>
                                    <li><a href="http://www.npf.gov.ng/">Nigeria</a></li>

                                    <li><a href="http://pol.gov.so/">Somalia</a></li>
                                    <li><a href="http://www.saps.gov.za/">South Africa</a></li>
                                    <li><a href="http://www.zrp.gov.zw/">Zimbabwe</a></li>
                                    <li><a href="https://www.idf.il/EN">Israel </a></li>
                                    <li><a href="http://www.police.go.kr/">South Korea</a></li>
                                    <li><a href="https://www.state.gov/">	United States</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bottom Footer Area -->
    <div class="bottom-footer-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-12">
                    <div class="copywrite-text">
                        <p style="opacity: 0;">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                            All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i> by <a
                                    href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>

                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            &copy;<script>document.write(new Date().getFullYear());</script>
                             International Media Corporation.All Rights Reserved
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End -->

<!-- jQuery (Necessary for All JavaScript Plugins) -->
<script src="{{asset('')}}assets/js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="{{asset('')}}assets/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="{{asset('')}}assets/js/bootstrap.min.js"></script>
<!-- Plugins js -->
<script src="{{asset('')}}assets/js/plugins.js"></script>
<!-- Active js -->
<script src="{{asset('')}}assets/js/active.js"></script>

<script>
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time-show').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        ;  // add zero in front of numbers < 10
        return i;
    }
</script>
</body>

