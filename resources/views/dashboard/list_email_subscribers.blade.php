@extends('layouts.dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                List Email Subscriptions
                <small>Email Subscriptions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Email Subscriptions</a></li>
                <li class="active">List Email Subscriptions</li>
            </ol>
        </section>
        <section class="content">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
        @endif
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">List Email Subscriptions</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row justify-content-center">
                        <div class="col-md-12">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Email ID</th>
                                    <th scope="col">Created at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($emails as $newsone)
                                    <tr>
                                        <th scope="row">{{$x}}</th>
                                        <td>{{$newsone->email}}</td>
                                        <td>{{$newsone->created_at}}</td>

                                    </tr>
                                    @php $x++; @endphp

                                @endforeach
                                </tbody>
                            </table>

                            {{$emails->links()}}

                        </div>
                    </div>

                </div>
                <!-- /.box -->

        </section>
        <div class="container">

        </div>
    </div>

@endsection

@section('extra-js')

@endsection
