@extends('layouts.dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Breaking News
                <small>Breaking News</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Breaking News</a></li>
                <li class="active">Add Breaking News</li>
            </ol>
        </section>
        <section class="content">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Title</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">Add Breaking News</div>

                                <div class="card-body">

                                    <form action="{{route('dashboard.save_breaking_news')}}" method="post"
                                          enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label for="">News Title</label>
                                            <input type="text" class="form-control" name="news_title">
                                        </div>

                                        <div class="form-group">
                                            <label for="">News Short Description</label>
                                            <textarea class="form-control" name="news_short_description" id=""
                                                      rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">News Long Description</label>
                                            <textarea class="form-control" name="news_long_description" id=""
                                                      rows="5"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="">News Thumbnail</label>
                                            <input type="file" class="form-control" name="image">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-success">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <div class="container">

        </div>
    </div>
@endsection
