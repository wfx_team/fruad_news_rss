<?php

namespace App\Http\Controllers;

use App\Models\MainNews;
use App\Repository\GrabDataRepositoryInterface;
use App\Services\Slug;
use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Http\Request;
use willvincent\Feeds\Facades\FeedsFacade;

class GrabController extends Controller
{
    private $grabDataRepository;

    //
    public function __construct(GrabDataRepositoryInterface $grabDataRepository)
    {
        $this->grabDataRepository = $grabDataRepository;
    }

    public function grabData()
    {
        $this->grabDataRepository->grabDataFromRSS();
    }

    public function grabSingleData()
    {
        $this->grabDataRepository->fetchDataByPost();
    }

    public function grabVideosFromYoutube()
    {
        $chanelID = 'UC16niRr50-MSBwiO3YDb3RA';
        $videos = $this->grabDataRepository->fetchVideoFromUrl($chanelID);
        dd($videos);
    }
}
