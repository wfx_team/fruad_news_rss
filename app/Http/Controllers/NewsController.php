<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use App\Repository\GrabDataRepositoryInterface;
use App\Repository\NewsDataRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Toolkito\Larasap\SendTo;


class NewsController extends Controller
{
    private $newsRepository;
    private $grabDataRepository;

    public function __construct(NewsDataRepositoryInterface $newsRepository, GrabDataRepositoryInterface $grabDataRepository)
    {
        $this->newsRepository = $newsRepository;
        $this->grabDataRepository = $grabDataRepository;
    }

    public function index()
    {
        $data = $this->newsRepository->getNewsData();
        $categories = $this->newsRepository->getNewsCategories();
        $latest_news = $this->newsRepository->getLatestNews();
        return view('pages.home', [
            'newsData' => $data,
            'categories' => $categories,
            'latest_news' => $latest_news,
        ]);
    }

    public function searchPage()
    {
        $term = Input::get('s');
        $search_data = $this->newsRepository->searchNewsData($term);
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();

        return view('pages.search',
            [
                'categories' => $categories,
                'latest_news' => $latest_news,
                'search_data' => $search_data,
            ]);
    }


    public function newsSingleView($slug)
    {
        $this->grabDataRepository->fetchDataBySlug($slug);

        $data = $this->newsRepository->newsSingle($slug);
        $entertainmentNews = $this->newsRepository->getEntertainmentNews();
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        $business = $this->newsRepository->getNewsByCategoryID(5,6);
        $world = $this->newsRepository->getNewsByCategoryID(2,5);
        $breaking = $this->newsRepository->getNewsByCategoryID(10,5);
        return view('pages.single_post', [

            'news' => $data,
            'latest_news' => $latest_news,
            'categories' => $categories,
            'entertainmentNews' => $entertainmentNews,
            'business' => $business,
            'world' => $world,
            'breaking' => $breaking,
        ]);
    }

    public function newsCategoryView($slug)
    {
        $news = $this->newsRepository->getNewsByCategory($slug);
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        $category = $this->newsRepository->getNewsCategoryFromSlug($slug);
//        dd($news);

        return view('pages.list_cat',
            [
                'categories' => $categories,
                'latest_news' => $latest_news,
                'news_data' => $news,
                'category' => $category,
            ]);
    }

    public function showPrivacyPolicy()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.privacy_policy', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }

    public function showAdvertise()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.advertise_with_us', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }

    public function showDisclaimer()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.disclaimer', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }
    public function showAboutUs()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.about_us', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }

    public function showContactUs()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.contact_us', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }
    public function showTerms()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.terms', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }
    public function showCookies()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.cookies', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }
    public function showPodcast()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.podcast', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }
    public function sendContactUs(Request $request)
    {
        $this->newsRepository->sendContactUs($request);
        return redirect()->back()->with('success', 'Thanks for contacting us. We will get back you soon.');
    }

    public function subscribe(Request $request){
        $res = $this->newsRepository->saveSubscribeEmailId($request->subs_email);
        if($res){
            return redirect()->back()->with('subs_success', 'Thank you for subscribing.');
        }else{
            return redirect()->back()->with('subs_error', 'Something went wrong');
        }

    }

    public function runAutoPost(){


        $this->newsRepository->runAutoPost();

        return response()->json('success');

//        $url = 'https://graph.facebook.com/v8.0/1231681657168056/accounts?
//            access_token=EAALEivJ8NnEBALGJt3JKaNMb1VNJl6phGY0suSkTi9rOhImwaZAdqZBdclNE53OYIGuTGmwz0Aku2C9Oq2ls3mWFMk6jKl2yBuOMXJYiqqjWH04i00lU0bcMj9cj5NcYGIlnmgBK1LTbiO9cNaByCRtVfanOaPcwrayvJZBCUm2KYWwROVxp50JGYH02vkZD';
//        $ch = curl_init();
//        curl_setopt($ch,CURLOPT_URL,$url);
////        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
//        $output=curl_exec($ch);
//
//        curl_close($ch);
//        return response()->json($output);


//
//        SendTo::Facebook(
//            'link',
//            [
//                'link' => 'https://github.com/toolkito/laravel-social-auto-posting',
//                'message' => 'Laravel social auto posting'
//            ]
//        );

    }


    public function submitStory()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        return view('pages.submit-story', [


            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }

    public function sendSubmitStory(Request $request)
    {
        $this->newsRepository->sendSubmitStory($request);
        return redirect()->back()->with('success', 'Thanks for Sending story us.');
    }


//    ----------------sitemap-----------------
    public function sitemap()
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();
        $dates = $this->newsRepository->getArticalMonths();
        return view('pages.sitemap', [
            'latest_news' => $latest_news,
            'categories' => $categories,
            'dates' => $dates,
        ]);
    }

    public function sitemapCategory($slug, $type = null)
    {
        $latest_news = $this->newsRepository->getLatestNews();
        $categories = $this->newsRepository->getNewsCategories();

        if ($type == 'category'):
            $news = $this->newsRepository->getNewsByCategory($slug, 500);
        else:
            $news = $this->newsRepository->getNewsByMonth($slug, 500);
        endif;
//        dd($news);
        return view('pages.sitemap-inner', [
            'news' => $news,
            'latest_news' => $latest_news,
            'categories' => $categories,
        ]);
    }

}
