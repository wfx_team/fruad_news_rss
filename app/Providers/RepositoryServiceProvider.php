<?php

namespace App\Providers;

use App\Repository\GrabDataRepositoryInterface;
use App\Repository\GrabRepository;
use App\Repository\NewsDataRepositoryInterface;
use App\Repository\NewsRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GrabDataRepositoryInterface::class, GrabRepository::class);
        $this->app->bind(NewsDataRepositoryInterface::class, NewsRepository::class);
    }
}
