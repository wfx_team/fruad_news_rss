<?php


namespace App\Repository;


interface NewsDataRepositoryInterface
{

    public function getNewsData();

    public function searchNewsData($term);

    public function newsSingle($slug);

    public function getNewsCategories();

    public function getNewsByCategory($category_slug);

    public function getLatestNews();

    public function getNewsCategoryFromSlug($slug);

    public function getEntertainmentNews();

    public function resize_image($request);

    public function saveBreakingNews($data);

    public function sendContactUs($data);

    public function saveSubscribeEmailId($email);

    public function runAutoPost();

    public function getNewsByCategoryID($cat_id, $limit);

    public function sendSubmitStory($data);

    public function getNewsByMonth($date, $per_page);

    public function getArticalMonths();
}
