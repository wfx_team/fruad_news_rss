<?php
/**
 * Created by PhpStorm.
 * User: pramod-pc
 * Date: 9/12/20
 * Time: 11:49 PM
 */

namespace App\Repository;


use App\Mail\SubmitStory;
use App\Models\Category;
use App\Models\EmailSubscription;
use App\Models\MainNews;
use App\Models\SourcePath;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use PhpParser\Node\Stmt\DeclareDeclare;
use Illuminate\Support\Facades\Mail;
use Toolkito\Larasap\SendTo;

class NewsRepository implements NewsDataRepositoryInterface
{
    public function getNewsData()
    {

        $newsData = (object)array(
            'politicsNews' => $this->getPoliticsNews(),
            'aniNews' => $this->getANINews(),
            'businessNews' => $this->getBusinessNews(),
            'scienceNews' => $this->getScienceNews(),
            'lifeNews' => $this->getLifeNews(),
            'latestNews' => $this->getLatestNews(),
            'latestNews_body' => $this->getLatestNews(4),
            'breakingNews' => $this->getBreakingNews(),
            'worldNews' => $this->getWorldNews(),
            'healthNews' => $this->getHealthNews(),
            'entertainmentNews' => $this->getEntertainmentNews(),
            'video' => $this->getNewsByCategoryID(11, 5),

        );
//dd($newsData);
        return $newsData;
    }

    public function getANINews()
    {
        return MainNews::where('status', 1)->where('source', 'ANINEWS')->limit(6)->orderBy('created_at', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getNewsByCategoryID($cat_id, $limit = 2)
    {
        $news_category = SourcePath::where('category_id', $cat_id)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit($limit)->orderBy('published_date', 'DESC')->select('title', 'short_description', 'slug', 'published_date', 'source', 'thumb')->get();

    }

    public function getBusinessNews()
    {
        $news_category = SourcePath::where('category_id', 5)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(5)->orderBy('published_date', 'DESC')->select('title', 'short_description', 'slug', 'published_date', 'source')->get();

    }

    public function getPoliticsNews()
    {

        $news_category = SourcePath::where('category_id', 4)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(2)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getScienceNews()
    {

        $news_category = SourcePath::where('category_id', 3)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(3)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getLifeNews()
    {

        $news_category = SourcePath::where('category_id', 9)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(5)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getWorldNews()
    {

        $news_category = SourcePath::where('category_id', 2)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(2)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getHealthNews()
    {

        $news_category = SourcePath::where('category_id', 6)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(6)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getEntertainmentNews()
    {

        $news_category = SourcePath::where('category_id', 7)->get()->pluck('source_type');
        return MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(6)->orderBy('published_date', 'DESC')->select('id', 'title', 'short_description', 'thumb', 'slug', 'published_date', 'source')->get();

    }

    public function getLatestNews($limit = 5)
    {

        $news_category = SourcePath::where('category_id', 1)->get()->pluck('source_type');
        $news = MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit($limit)->orderBy('published_date', 'DESC')->select('title', 'slug', 'published_date', 'source', 'thumb')->get();
        return $news;
    }

    public function getBreakingNews()
    {
        $news_category = SourcePath::where('category_id', 10)->get()->pluck('source_type');
        $news = MainNews::where('status', 1)->whereIn('source_type', $news_category)->limit(3)->orderBy('published_date', 'DESC')->select('title', 'slug', 'published_date', 'source', 'thumb', 'short_description')->get();
        return $news;
    }

    public function searchNewsData($term)
    {

        $res = DB::table('main_news as nm')
            ->where('nm.status', 1)
            ->Where('nm.title', 'like', '%' . $term . '%')
            ->orWhere('nm.short_description', 'like', '%' . $term . '%')
//            ->orWhere('long_description','like','%'.$term.'%')
            ->orderBy('nm.published_date', 'DESC')
            ->leftJoin('source_paths as sp', 'nm.source_type', 'sp.source_type')
            ->leftJoin('categories as cat', 'sp.category_id', 'cat.id')
            ->select('nm.*', 'sp.category_id as category_id', 'cat.name as cat_name', 'cat.slug as cat_slug')
            ->distinct()
            ->paginate(15)
            ->appends(['s' => $term]);
        return $res;

    }

    public function newsSingle($slug)
    {

        $res = MainNews::where('slug', $slug)
            ->where('status', 1)
            ->first();

        if (!$res):
            abort(404);
        endif;

        return $res;
    }

    public function getNewsCategories()
    {
        return Category::orderBy('sequence', 'ASC')->get();
    }

    public function getNewsByCategory($category_slug)
    {
        $category_id = Category::where('slug', $category_slug)->first();
        $news_category = SourcePath::where('category_id', $category_id->id)->get()->pluck('source_type');

        $news = DB::table('main_news as nm')
            ->where('nm.status', 1)
            ->whereIn('nm.source_type', $news_category)
            ->leftJoin('source_paths as sp', 'nm.source_type', 'sp.source_type')
            ->leftJoin('categories as cat', 'sp.category_id', 'cat.id')
            ->select('nm.*', 'sp.category_id as category_id', 'cat.name as cat_name', 'cat.slug as cat_slug')
            ->distinct()
            ->paginate(15);

        return $news;
    }


    public function getNewsCategoryFromSlug($slug)
    {
        return Category::where('slug', $slug)->select('name', 'slug')->first();
    }

    public function resize_image($request)
    {
        $dir = './thumbnail';
        $file = $request->image; //file field
        $ext = $file->guessExtension(); //get file extenstion
        $name = 'que-' . uniqid() . '.' . $ext; //create file name
        $res = Input::file('image')->move($dir . '/', $name); //path to save file


        return '/thumbnail/' . $name;

    }

    public function saveBreakingNews($data)
    {
        $res = MainNews::create($data);

        return $res;
    }

    public function sendContactUs($details)
    {

        \Mail::to('info@fraudsbraking.com')->send(new \App\Mail\ContactUsMail($details->data));

    }

    public function saveSubscribeEmailId($email)
    {
        $res = EmailSubscription::create(['email' => $email]);
        return $res;
    }

    public function runAutoPost()
    {

        $data = MainNews::where('status', 1)->where('auto_post', 0)->orderBy('published_date', 'DESC')->limit(2)->get();
        if (!empty($data)) {
            foreach ($data as $post) {
                     $res = SendTo::Facebook(
                          'link',
                          [
                              'link' => 'https://fraudsbreaking.com/news/' . $post->slug,
                              'message' => $post->title
                          ]
                      );

                $res = SendTo::Twitter(
                    $post->title.'.'.'https://fraudsbreaking.com/news/'. $post->slug
                );

                if ($res == true) {

                    MainNews::where('slug', $post->slug)->update([
                        'auto_post' => 1
                    ]);


                } else {

                    return response()->json('Auto Post fail');
                }
            }

            return response()->json('Auto Post Success');
        } else {
            return response()->json('No Pending Post');
        }

    }


    public function sendSubmitStory($details)
    {
        $images = [];
        $govDoc = [];

        if (count($details->data['image'])) {
//            dump('has');
            foreach ($details->data['image'] as $image) {
                array_push($images, $this->fileUploadPost($image));

            }
        }
        if (count($details->data['gov_doc'])) {
//            dump('has');
            foreach ($details->data['gov_doc'] as $image) {
                array_push($govDoc, $this->fileUploadPost($image));

            }
        }
//        dump($images);
        $data = array(
            'salutations' => $details->data['salutations'],
            'name' => $details->data['name'],
            'email' => $details->data['email'],
            'phone' => $details->data['phone'],
            'countryCode' => $details->data['countryCode'],
            'are_you' => $details->data['are_you'],
            'subject' => $details->data['subject'],
            'brief' => $details->data['brief'],
            'evidence' => $details->data['evidence'],
            'evidence_how' => $details->data['evidence_how'],
            'secret' => $details->data['secret'],
            'in_detail' => $details->data['subject'],
            'pay' => $details->data['pay'],
            'broadcasted' => $details->data['broadcasted'],
            'address' => $details->data['address'],
            'image' => $images,
            'gov_doc' => $govDoc,

        );

        \Mail::to('contact@scamsbreaking.com')->send(new SubmitStory($data));

    }

    private function fileUploadPost($request)
    {
        $fileName = time() . '.' . $request->extension();

        $request->move(public_path('uploads'), $fileName);

        return $fileName;

    }

//------------sitemap-------
    public function getArticalMonths()
    {

        $res = MainNews::select('created_at')->get();
        $all = $res->map(function ($item, $key) {
            return $this->format($item);
        });

        return array_keys($all->groupBy('date')->toArray());

    }

    protected function format($item)
    {

        $data = array(
            'date' => Carbon::parse($item->created_at)->format('Y F'),
        );

        return $data;

    }


    public function getNewsByMonth($date, $per_page = 15)
    {
        $news_category = SourcePath::whereMonth('created_at', Carbon::parse($date))->get()->pluck('source_type');

        $news = DB::table('main_news as nm')
            ->where('nm.status', 1)
            ->whereMonth('nm.created_at', Carbon::parse($date)->month)
//            ->leftJoin('source_paths as sp', 'sp.source_type','=', 'nm.source_type')
//            ->leftJoin('categories as cat', 'sp.category_id', 'cat.id')
            ->select('nm.*')
            ->distinct()
            ->paginate($per_page);

        return $news;
    }
}
