<?php


namespace App\Repository;


interface GrabDataRepositoryInterface
{
    public function grabDataFromRSS();

    public function fetchDataByPost();

    public function fetchDataBySlug($slug);

    public function fetchVideoFromUrl($chanelID);

}