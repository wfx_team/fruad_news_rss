<?php


namespace App\Repository;


use App\Models\FetchDetail;
use App\Models\MainNews;
use App\Models\SitemapGeneration;
use App\Models\SourcePath;
use App\Services\Slug;
use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Spatie\Sitemap\SitemapGenerator;
use willvincent\Feeds\Facades\FeedsFacade;

class GrabRepository implements GrabDataRepositoryInterface
{

    public function grabDataFromRSS()
    {
        $count = 1;
        foreach ($this->getSourceData() as $sourcePath):
            $single_count = 0;
            $feed = FeedsFacade::make($sourcePath->url);

            $source_title = $feed->get_title();
            $source_thumb = $feed->get_image_url();
            $post_count = $feed->get_item_quantity();

            $items = $feed->get_items(); //grab all items inside the rss


            foreach ($items as $item):
                $slug = new Slug();

                $title = $item->get_title();
                $slug = $slug->createSlug($title);
                $url = $item->get_permalink();

                $enclosure = $item->get_enclosures();

                $arr = [];
                foreach ($enclosure as $enc):
                    array_push($arr, $enc->get_link());
                endforeach;
                $other_images = json_encode($arr);

                $description = $item->get_description(); //get the link of single news
                $published_at = ($item->get_date()) ? Carbon::parse($item->get_date())->format('Y-m-d H:i:s') : null;

                $enclosure = $item->get_enclosure();
                $thumb = $enclosure->get_link();

                $data = array(
                    'title' => $title,
                    'slug' => $slug,
                    'short_description' => $description,
                    'thumb' => $thumb,
                    'other_images' => $other_images,
                    'published_date' => $published_at,
                    'original_url' => $url,
                    'source' => $sourcePath->source,
                    'source_type' => $sourcePath->source_type
                );


                if (!$this->checkDuplicatesInDB($data)):
                    $single_count++;
                    $res = MainNews::create($data);
//                    dump($count);
                    $count++;
                endif;

            endforeach;
            if ($single_count > 0):
                $data = array(
                    'source' => $sourcePath->source,
                    'source_type' => $sourcePath->source_type,
                    'fetched_path' => $sourcePath->url,
                    'added_records' => $single_count
                );
                FetchDetail::create($data);

            endif;
        endforeach;


    }

    private function getSourceData()
    {
        return SourcePath::where('status', 1)->get();
    }

    private function checkDuplicatesInDB($data)
    {
        $res = MainNews::where('title', 'like', '%' . $data['title'] . '%')
            ->where('source', $data['source'])
            ->count();
        return $res;
    }

    public function fetchDataByPost()
    {
        $items = MainNews::where('is_fetch_all_info', 0)->where('status', 1)->limit(10)->get();
        foreach ($items as $item):
            try {
                $this->saveGrabSingleData($item->id, $this->grabSinglePostData($item));
            } catch (QueryException $ex) {

            }
        endforeach;
    }

    public function fetchDataBySlug($slug)
    {
        $item = MainNews::where('is_fetch_all_info', 0)->where('status', 1)->where('slug', $slug)->first();
        if ($item):
            try {
                $this->saveGrabSingleData($item->id, $this->grabSinglePostData($item));
            } catch (QueryException $ex) {
                $this->saveGrabSingleData($item->id, $this->grabSinglePostDataForText($item));
            }
        endif;
    }

    private function grabSinglePostData($data)
    {
        $client = new Client();
        $crawler = $client->request('GET', $data->original_url);
//        dump($data->original_url);
        $html = '';
        $css_class = array(
            '.pg .l-container',
            '.ls-main',
            '.BasicArticle__component ',
            '.zn-body-text',
            '.Article__zone',
            '.zn-large-media',
            '.main-content .article-wrap',
            '.article-page-section .main-container',
//            '.configurable .column--primary .story-body',
            '#root #main-content article',
            '.wd_main_content #wd_printable_content',
            '.configurable .column--primary',
            '.main-wrapper .news-article #news-detail-block .card'
        );
        foreach ($css_class as $one):
            $html .= $this->crawlData($crawler, $one);
//        dump($one);
        endforeach;

        return $html;

    }

    private function crawlData($crawler, $css_element)
    {
        $test = '';
        $crawler->filter($css_element)->each(function ($node) use (&$test) {
//            dd($node->html());
            $test = $node->html() . "\n";
        });
        return $test;
    }

//-----------

    private function grabSinglePostDataForText($data)
    {
        $client = new Client();
        $crawler = $client->request('GET', $data->original_url);
//        dump($data->original_url);
        $html = '';
        $css_class = array(
            '.pg .l-container',
            '.ls-main',
            '.BasicArticle__component ',
            '.zn-body-text',
            '.Article__zone',
            '.zn-large-media',
            '.main-content .article-wrap',
            '.article-page-section .main-container',
//            '.configurable .column--primary .story-body',
            '#root #main-content article',
            '.wd_main_content #wd_printable_content',
            '.configurable .column--primary',
            '.main-wrapper .news-article #news-detail-block .card'
        );
        foreach ($css_class as $one):
            $html .= $this->crawlDataAndgetText($crawler, $one);
//        dump($one);
        endforeach;

        return $html;

    }

    private function crawlDataAndgetText($crawler, $css_element)
    {
        $test = '';
        $crawler->filter($css_element)->each(function ($node) use (&$test) {
//            dd($node->html());
            $test = $node->text() . "\n";
        });
        return $test;
    }


    private function saveGrabSingleData($id, $html)
    {
        $res = MainNews::where('id', $id)->update(['long_description' => base64_encode($html), 'is_fetch_all_info' => 1]);
        return $res;
    }

    public function fetchVideoFromUrl($chanelID)
    {
        $header = array('Authorization' => 'token');
        $url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDbPBhhPX6oL3rqtbLt5jDMHPgBSkDt_I0&channelId=' . $chanelID . '&part=snippet,id&order=date&maxResults=20';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url, array('headers' => $header));
        $data = $response;
        $content = json_decode($response->getBody(), true);
        $single_count = 0;

        foreach ($content['items'] as $item):
            $slug = new Slug();
            $title = $this->clean($item['snippet']['title']);
            $slug = $slug->createSlug($item['snippet']['title']);
            $data = array(
                'title' => $title,
                'slug' => $slug,
                'short_description' => Str::limit($item['snippet']['description'], 200),
                'thumb' => $item['snippet']['thumbnails']['medium']['url'],
                'other_images' => json_encode($item['snippet']['thumbnails']),
                'published_date' => Carbon::parse($item['snippet']['publishedAt'])->format('Y-m-d H:i:s'),
                'original_url' => 'https://www.youtube.com/embed/' . $item['id']['videoId'],
                'source' => 'admin',
                'source_type' => 'Video'
            );
            if (!$this->checkDuplicatesInDB($data)):
                $single_count++;
                $res = MainNews::create($data);
//                    dump($count);

            endif;
        endforeach;
        if ($single_count > 0):
            $data = array(
                'source' => 'admin',
                'source_type' => 'Video',
                'fetched_path' => 'Youtube',
                'added_records' => $single_count
            );
            FetchDetail::create($data);
        endif;

    }

    private function clean($string)
    {
        $string = str_replace('&#39;', ' ', $string); // Replaces all spaces with hyphens.

        return $string;
    }

    public function sitemapGenerate()
    {
        SitemapGenerator::create(asset(''))->writeToFile(public_path() . '/sitemap.xml');

        SitemapGeneration::create(['sitemap_name'=>'sitemap.xml','site'=>'fraudsbreaking.com']);

    }
}
