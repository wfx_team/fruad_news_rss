<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitemapGeneration extends Model
{
    protected $table ='sitemap_generation';
    protected $fillable = ['sitemap_name','site'];
}
