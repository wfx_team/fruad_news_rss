<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FetchDetail extends Model
{
    //
    protected $fillable = ['source','source_type','fetched_path','added_records'];

}
