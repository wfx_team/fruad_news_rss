<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainNews extends Model
{
    //
    protected $table = 'main_news';
    protected $fillable = ['title', 'slug', 'short_description', 'long_description', 'thumb', 'other_images',
        'published_date', 'original_url', 'source', 'source_type','status','is_fetch_all_info'];
}
