<?php

namespace App\Console\Commands;

use App\Models\FetchDetail;
use App\Models\MainNews;
use App\Models\SourcePath;
use App\Services\Slug;
use Carbon\Carbon;
use Illuminate\Console\Command;
use willvincent\Feeds\Facades\FeedsFacade;

class GrabPostCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grab:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab all post from RSS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Grab Cron is working fine!");
        $count = 1;
        foreach ($this->getSourceData() as $sourcePath):
            $single_count = 0;
            $feed = FeedsFacade::make($sourcePath->url);

            $source_title = $feed->get_title();
            $source_thumb = $feed->get_image_url();
            $post_count = $feed->get_item_quantity();

            $items = $feed->get_items(); //grab all items inside the rss


            foreach ($items as $item):
                $slug = new Slug();

                $title = $item->get_title();
                $slug = $slug->createSlug($title);
                $url = $item->get_permalink();

                $enclosure = $item->get_enclosures();

                $arr = [];
                foreach ($enclosure as $enc):
                    array_push($arr, $enc->get_link());
                endforeach;
                $other_images = json_encode($arr);

                $description = $item->get_description(); //get the link of single news
                $published_at = ($item->get_date()) ? Carbon::parse($item->get_date())->format('Y-m-d H:i:s') : null;

                $enclosure = $item->get_enclosure();
                $thumb = $enclosure->get_link();

                $data = array(
                    'title' => $title,
                    'slug' => $slug,
                    'short_description' => $description,
                    'thumb' => $thumb,
                    'other_images' => $other_images,
                    'published_date' => $published_at,
                    'original_url' => $url,
                    'source' => $sourcePath->source,
                    'source_type' => $sourcePath->source_type
                );


                if (!$this->checkDuplicatesInDB($data)):
                    $single_count++;
                    $res = MainNews::create($data);
//                    dump($count);
                    $count++;
                endif;

            endforeach;
            if ($single_count > 0):
                $data = array(
                    'source' => $sourcePath->source,
                    'source_type' => $sourcePath->source_type,
                    'fetched_path' => $sourcePath->url,
                    'added_records' => $single_count
                );
                FetchDetail::create($data);

            endif;
        endforeach;


        $this->info('Grab Cron Command Run successfully!');
    }
    private function getSourceData()
    {
        return SourcePath::where('status', 1)->get();
    }
    private function checkDuplicatesInDB($data)
    {
        $res = MainNews::where('title', 'like', '%' . $data['title'] . '%')
            ->where('source', $data['source'])
            ->count();
        return $res;
    }
}
