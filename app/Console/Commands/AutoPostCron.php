<?php

namespace App\Console\Commands;

use App\Models\MainNews;
use Illuminate\Console\Command;
use Toolkito\Larasap\SendTo;

class AutoPostCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autopost:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post Article to Social Media Pages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");

        $data = MainNews::where('status', 1)->where('auto_post', 0)->orderBy('published_date', 'DESC')->limit(2)->get();
        if (!empty($data)) {

            foreach ($data as $post) {

                $res = SendTo::Facebook(
                    'link',
                    [
                        'link' => 'https://fraudsbreaking.com/news/' . $post->slug,
                        'message' => $post->title
                    ]
                );

                SendTo::Twitter(
                    $post->title.'.'.'https://fraudsbreaking.com/news/'. $post->slug
                );

                if ($res == true) {

                    MainNews::where('slug', $post->slug)->update([
                        'auto_post' => 1
                    ]);
                }
            }
        }
        $this->info('Cron Command Run successfully!');
    }
}
