<?php

namespace App\Console\Commands;

use App\Http\Controllers\GrabController;
use App\Repository\GrabRepository;
use Illuminate\Console\Command;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Generating SiteMap');

        $grab = new GrabRepository();
        $grab->sitemapGenerate();

        $this->info('sitemap.xml Stored in Public folder');
    }
}
