<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//backend task
Route::get('/grab-data', 'GrabController@grabData');
Route::get('/grab-single-data', 'GrabController@grabSingleData');
Route::get('/grab-videos', 'GrabController@grabVideosFromYoutube');
Route::get('/sitemap-gen', 'GrabController@sitemapGenerate');

//frontend
Route::get('/', 'NewsController@index')->name('site.index');
Route::get('/search', 'NewsController@searchPage')->name('site.search');
Route::get('/news/{slug}', 'NewsController@newsSingleView')->name('site.singleView');
Route::get('/category/{slug}', 'NewsController@newsCategoryView')->name('site.categoryView');
Route::get('/privacy-policy', 'NewsController@showPrivacyPolicy')->name('site.privacy');
Route::get('/advertise-with-us', 'NewsController@showAdvertise')->name('site.advertise');
Route::get('/disclaimer', 'NewsController@showDisclaimer')->name('site.disclaimer');
Route::get('/about-us', 'NewsController@showAboutUs')->name('site.about');
Route::get('/terms', 'NewsController@showTerms')->name('site.terms');
Route::get('/contact-us', 'NewsController@showContactUs')->name('site.contactUs');
Route::post('/send-mail', 'NewsController@sendContactUs')->name('site.sendContactUs');
Route::post('/subscribe', 'NewsController@subscribe')->name('site.subscribe');
Route::get('/submit-your-story', 'NewsController@submitStory')->name('site.submitStory');
Route::post('/send-mail-story', 'NewsController@sendSubmitStory')->name('site.sendSubmitStory');

Route::get('/sitemap', 'NewsController@sitemap')->name('site.sitemap');
Route::get('/sitemap/{slug}/{type}', 'NewsController@sitemapCategory')->name('site.sitemapCategory');

Route::get('/cookies', 'NewsController@showCookies')->name('site.cookies');
Route::get('/podcast', 'NewsController@showPodcast')->name('site.podcast');

Route::get('/auto-post', 'NewsController@runAutoPost')->name('site.autopost');



Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard/add-breaking-news', 'HomeController@add_breaking_news')->name('dashboard.add_breaking_news');
    Route::post('/dashboard/add-breaking-news/save', 'HomeController@save_breaking_news')->name('dashboard.save_breaking_news');
    Route::get('/dashboard/list-breaking-news', 'HomeController@list_breaking_news')->name('dashboard.list_breaking_news');
    Route::get('/dashboard/{slug}/delete', 'HomeController@delete_breaking_news')->name('dashboard.delete_breaking_news');
    Route::get('/dashboard/email-subscribers/list', 'HomeController@email_subscribers')->name('dashboard.email_subscribers');

});
